from flask import Flask, jsonify, request

""" For flask to know that this app is running uniquely """
app = Flask(__name__) # Create an app from flask

stores = [
  {
    "name": "My wonderful store",
    "items": [
      {
        "name": "My Item",
        "price": 30.89
      }
    ]
  }
]

@app.route('/') # Declaring a route for our application
def home(): # Method for route to match to
  return "Hellow, world!"

# POST /store data { name: }
@app.route('/store', methods=['POST'])
def create_store():
  request_data = request.get_json() # get json from request
  new_store = {
    'name': request_data['name'],
    'items': []
  }

  stores.append(new_store)
  return jsonify(new_store)

# GET /store/<string:name/
@app.route('/store/<string:name>')
def get_store(name):
  # Iterate over stores, if there's a match return it

  for store in stores:
    if store['name'] == name:
      return jsonify(store)

  return jsonify({ 'error': 'No match found'})

# GET /store
@app.route('/store')
def get_stores():
  return jsonify({'stores': stores})

# POST /store/<string:name/item
@app.route('/store/<string:name>/item', methods=['POST'])
def create_store_item(name):
  request_data = request.get_json() # get json from request

  for store in stores:
    if store['name'] == name:
      new_item = {
        'name': request_data['name'],
        'price': request_data['price']
      }
      store['items'].append(new_item)
      return jsonify(store)

  return jsonify({ 'error': 'No match found'})


# GET /store/<string:name/item
@app.route('/store/<string:name>/item')
def get_store_item(name):
  for store in stores:
    if store['name'] == name:
      return jsonify({'items': store['items']})

  return jsonify({ 'error': 'No match found'})

app.run(port=5000) # Set app to run on port 5000